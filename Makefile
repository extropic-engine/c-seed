
include config.mk

PROJ = cseed
SRC = main.c
OBJ = ${SRC:.c=.o}

all: options cseed

options:
	@echo ${PROJ} build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	@echo CC $<
	@${CC} -c ${CFLAGS} $<

cseed: main.o
	@echo CC -o $@
	@${CC} -o $@ main.o ${LDFLAGS}

clean:
	@echo cleaning
	@rm -f ${PROJ} ${OBJ} ${PROJ}-${VERSION}.tar.gz

dist: clean
	@echo creating dist tarball
	@mkdir -p ${PROJ}-${VERSION}
	@cp LICENSE Makefile README ${SRC} \
		${PROJ}-${VERSION}
	@tar -cf ${PROJ}-${VERSION}.tar ${PROJ}-${VERSION}
	@gzip ${PROJ}-${VERSION}.tar
	@rm -rf ${PROJ}-${VERSION}

install: all
	@echo installing executables to ${DESTDIR}${PREFIX}/bin
	@mkdir -p ${DESTDIR}${PREFIX}/bin
	@cp -f ${PROJ} ${DESTDIR}${PREFIX}/bin
	@chmod 755 ${DESTDIR}${PREFIX}/bin/${PROJ}
	@echo installing manual pages to ${DESTDIR}${MANPREFIX}/man1
	@mkdir -p ${DESTDIR}${MANPREFIX}/man1
	@sed "s/VERSION/${VERSION}/g" < ${PROJ}.1 > ${DESTDIR}${MANPREFIX}/man1/${PROJ}.1
	@chmod 644 ${DESTDIR}${MANPREFIX}/man1/${PROJ}.1

uninstall:
	@echo removing executables from ${DESTDIR}${PREFIX}/bin
	@rm -f ${DESTDIR}${PREFIX}/bin/${PROJ}
	@echo removing manual page from ${DESTDIR}${MANPREFIX}/man1
	@rm -f ${DESTDIR}${MANPREFIX}/man1/${PROJ}.1

.PHONY: all options clean dist install uninstall
